module.exports = async (app, config) => {
  var express = require('express')

  var debug = require('debug')(`app:savoietc ${`${Date.now()}`.white}`)
  var builder = app.builder

  var COMMON_CONTEXT = {
    head: {
      titlePrefix: `Savoie Tech Coop`,
      title: '',
      imageUrl: config.getRouteName('/static/stc-share.jpg')
    },
    isDevelopment: process.env.NODE_ENV !== 'production'
  }
  if (!COMMON_CONTEXT.isDevelopment) {
    COMMON_CONTEXT.head.imageUrl =
      'https://savoie.misitioba.com/savoietc/static/stc-share.jpg'
  }

  function objectMerge(self, savedData) {
    if (savedData === undefined) {
      return
    }
    Object.keys(self).forEach(k => {
      if (typeof self[k] === 'object' && !(self[k] instanceof Array)) {
        objectMerge(self[k], savedData[k])
      } else {
        self[k] = savedData[k] || self[k]
      }
    })
    Object.keys(savedData)
      .filter(k => Object.keys(self).indexOf(k) == -1)
      .forEach(newK => {
        self[newK] = savedData[newK]
      })
  }

  function getCommonContext(ctx) {
    let baseCtx = require('lodash').cloneDeep(COMMON_CONTEXT)
    // let baseCtx = Object.assign({}, COMMON_CONTEXT)
    objectMerge(baseCtx, ctx)
    return baseCtx
  }
  let funql = require('funql-api')
  await funql.loadFunctionsFromFolder({
    namespace: config.name,
    path: config.getPath('api'),
    params: [app],
    scope: {
      dbName: config.db_name
    }
  })

  app.get(
    config.getRouteName('contact.js'),
    app.webpackMiddleware({
      entry: config.getPath('js/contact.js')
    })
  )
  app.get(
    config.getRouteName('home.js'),
    app.webpackMiddleware({
      entry: config.getPath('js/home.js')
    })
  )

  app.use(
    config.getRouteName('/static'),
    express.static(config.getPath('static'))
  )
  app.use('/res', express.static(config.getPath('pages')))

  app.get(
    '/',
    builder.transformFileRoute({
      cwd: config.getPath(),
      dist: '/dist',
      source: 'pages/home/home.pug',
      transform: [app.cacheCDNScripts],
      context: getCommonContext({
        cwd: config.getRouteName(),
        head: {
          title: 'Outils Web éthiques gratuits et décentralisés'
        }
      })
    })
  )

  app.get(
    config.getRouteName('backoffice.js'),
    app.webpackMiddleware({
      entry: config.getPath('js/backoffice.js')
    })
  )
  app.get(
    '/organizations/:name',
    builder.transformFileRoute({
      cwd: config.getPath(),
      dist: '/dist',
      source: 'pages/backoffice.pug',
      transform: [app.cacheCDNScripts],
      context: getCommonContext({
        cwd: config.getRouteName(),
        head: {
          title: 'Backoffice'
        }
      })
    })
  )

  app.get(
    '/contact',
    builder.transformFileRoute({
      cwd: config.getPath(),
      dist: '/dist',
      source: 'pages/contact.pug',
      transform: [app.cacheCDNScripts],
      context: getCommonContext({
        cwd: config.getRouteName(),
        head: {
          title: 'Contact'
        }
      })
    })
  )

  app.get(
    '/outils-web/reservation-de-paniers',
    builder.transformFileRoute({
      cwd: config.getPath(),
      dist: '/dist',
      source: 'pages/outils-web--reservation-de-paniers.pug',
      transform: [app.cacheCDNScripts],
      context: getCommonContext({
        cwd: config.getRouteName(),
        head: {
          title: 'Réservation paniers - Cooperatives de maraîchage'
        }
      })
    })
  )

  app.get(
    '/les-outils-web',
    builder.transformFileRoute({
      cwd: config.getPath(),
      dist: '/dist',
      source: 'pages/les-outils-web.pug',
      transform: [app.cacheCDNScripts],
      context: getCommonContext({
        cwd: config.getRouteName(),
        head: {
          title: `Les outils web`
        }
      })
    })
  )

  app.get(
    '/about',
    builder.transformFileRoute({
      cwd: config.getPath(),
      dist: '/dist',
      source: 'pages/about.pug',
      transform: [app.cacheCDNScripts],
      context: getCommonContext({
        cwd: config.getRouteName(),
        head: {
          title: `About`
        }
      })
    })
  )


  app.get(
    '/legal-mentions',
    builder.transformFileRoute({
      cwd: config.getPath(),
      dist: '/dist',
      target: '/legal-mentions/index.html',
      source: 'pages/legal-mentions.pug',
      mode: 'pug',
      transform: [app.cacheCDNScripts],
      context: getCommonContext({
        cwd: config.getRouteName(),
        head: {
          title: 'Mention Legal'
        }
      })
    })
  )
}