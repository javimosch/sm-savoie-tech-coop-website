module.exports = app => {
        var debug = require('debug')(`app:api:contactForm ${`${Date.now()}`.white}`)

  return async function contactForm (form) {
    sendMailgun({
      from: 'SAVOIETC <no-reply@misitioba.com>',
      to: 'arancibiajav@gmail.com',
      subject: 'SAVOIETC Contact form triggered',
      text: `
        EMAIL: ${form.email}
        MESSAGE: ${form.message}
        `
    })
    return await app.dbExecute(
      `
        INSERT INTO contact_messages
        (name,email,message,phone,creation_date)
        VALUES(?,?,?,?,?)
        `,
      [
        form.name || '',
        form.email || '',
        form.message || '',
        form.phone || '',
        form.creation_date
      ],
      {
        //dbName: this.dbName
      }
    )
  }

  function sendMailgun (data) {
    return new Promise(async (resolve, reject) => {
      /*
            var data = {
                from: 'misitioba (Tech Cooperative at Savoie) <no-reply@misitioba.com>',
                to: 'arancibiajav@gmail.com',
                subject: 'Test email',
                text: 'Testing some Mailgun awesomeness!'
              };
              */
      let mailgun = await app.getMailgun()
      mailgun.messages().send(data, function (error, body) {
        if (error) {
          debug(`contact`, error)
          reject(error)
        } else {
          resolve(true)
        }
      })
    })
  }
}