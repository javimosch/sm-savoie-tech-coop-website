new Vue({
    el: '.app',
    data() {
        return {
            form: {
                email: ''
            }
        }
    },
    created() { },
    methods: {
        TrackMoreInfo(moduleName) {
            analytics.track('ClickedMoreInfo', { moduleName })
            murlytics.track({ name: 'ClickedMoreInfo', moduleName })
        },
        TryDemo() {
            analytics.track('ClickedTryBasketHotDemo', {})
            murlytics.track({ name: 'ClickedTryBasketHotDemo' })
        },
        async requestDemo() {
            murlytics.track({ name: 'ClickedRequestDemo', email: this.form.email })
            analytics.track('ClickedRequestDemo', {
                email: this.form.email
            })
            if (!this.form.email) {
                alert('Nous avons besoin de votre email pour vous contacter')
                return
            }

            api.funql({
                namespace: 'savoietc',
                name: 'contactForm',
                args: [
                    Object.assign({}, this.form, {
                        message: 'HOME_PAGE_CTA_BUTTON',
                        creation_date: Date.now()
                    })
                ]
            })

            this.form.email = ''
            alert('Merci!, nous vous contacterons bientôt.')
        }
    }
})