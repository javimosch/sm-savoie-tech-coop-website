import ImportCDNJS from 'import-cdn-js'

// import './components/basketDetails'

import Dashboard from './backoffice/dashboard'

ImportCDNJS('/analytics.js')

window.ERROR = {
    API: 'Erreur de serveur ou de connexion'
}
const routes = [{ path: '/', component: Dashboard, name: 'dashboard' }]
const router = new VueRouter({
    routes
})

new Vue({
    name: 'app',
    el: '.app',
    router,
    data() {
        return {}
    },
    created() {
        console.log('APP created')
    }
})